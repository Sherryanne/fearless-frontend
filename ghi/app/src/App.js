import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from "./LocationForm"
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from "./PresentationForm";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import MainPage from './mainpage';



function App(props) {
  if(props.attendees === undefined) {
    return null;
  }
  return(
    <BrowserRouter>
    <Nav />
    <div className='container'>
      <Routes>
      <Route path="/" element={<MainPage/>} /> 
        <Route path="locations">
          <Route path="new" element={<LocationForm/>} />
        </Route>
        <Route path="conference">
          <Route path="con" element={<ConferenceForm/>} /> 
        </Route>
        <Route path="attendees">
          <Route path="new" element={<AttendConferenceForm/>} /> 
        </Route>
      </Routes>
    </div>  
    </BrowserRouter>
  );
}

export default App;